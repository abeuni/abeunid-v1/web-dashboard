import 'firebase/firebase-firestore'

import axios from 'axios'
import parse from 'csv-parse'
import firebase from 'firebase/app'
import fuzzysort from 'fuzzysort'
import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

const db = firebase
  .initializeApp({
    apiKey: 'AIzaSyA1ecjbVy8vFNhy9AWgEAFQlG41WLpn8eQ',
    authDomain: 'caravana-system.firebaseapp.com',
    databaseURL: 'https://caravana-system.firebaseio.com',
    projectId: 'caravana-system',
    storageBucket: 'caravana-system.appspot.com',
    messagingSenderId: '236368191983',
    appId: '1:236368191983:web:3e9c8004cc77afa3'
  })
  .firestore()

export default new Vuex.Store({
  plugins: [
    createPersistedState({
      reducer: persistedState => {
        const stateFilter = Object.assign({}, persistedState)
        const blackList = ['CEPData']

        blackList.forEach(item => {
          delete stateFilter[item]
        })

        return stateFilter
      }
    })
  ],
  state: { registered: [], assigned: [], CEPData: null },
  getters: {
    registeredList (state) {
      return state.registered
    },
    assignedList (state) {
      return state.assigned
    },
    streetList (state) {
      return state.CEPData
    }
  },
  mutations: {
    clear (state) {
      state.registered = []
      state.assigned = []
    },
    clearPeople (state) {
      state.registered = []
    },
    clearAssignments (state) {
      state.assigned = []
    },
    setCEPData (state, data) {
      state.CEPData = data
    },
    registerPerson (state, data) {
      state.registered.unshift({
        metadata: { uploaded: false, origin: 'local' },
        data
      })
    },
    uploadedPerson (state, index) {
      state.registered[index].metadata.uploaded = true
    },
    downloadedPerson (state, data) {
      state.registered.unshift({ metadata: { origin: 'remote' }, data })
    },
    assignCode (state, payload) {
      const { code, id, ref } = payload

      state.assigned.unshift({
        metadata: { uploaded: false, origin: 'local' },
        data: { id, code, ref, timestamp: new Date().toISOString() }
      })
    },
    uploadedAssignment (state, index) {
      state.assigned[index].metadata.uploaded = true
    },
    downloadedAssignment (state, data) {
      state.assigned.unshift({ metadata: { origin: 'remote' }, data })
    },
    sortAssignments (state, data) {
      state.assigned.sort((a, b) => {
        if (a.data.timestamp < b.data.timestamp) {
          return 1
        } else if (a.data.timestamp === b.data.timestamp) {
          return 0
        } else {
          return -1
        }
      })
    }
  },
  actions: {
    async loadCEP ({ commit, state }) {
      if (!state.CEPData) {
        axios.get('db/sp.saopaulo.cepaberto.csv').then(csvfile => {
          parse(csvfile.data, {}, (_, parsedcsv) => {
            const streets = parsedcsv.map(item => ({
              CEP: item[0],
              street: item[1],
              suburb: item[2],
              searchPrep: fuzzysort.prepare(
                (item[1] + ' ' + item[2])
                  .normalize('NFD')
                  .replace(/[\u0300-\u036f]/g, '')
              )
            }))

            commit('setCEPData', streets)
          })
        })
      }
    },
    async sync ({ commit, state }) {
      // Send changes to DB
      const cachedRegistered = state.registered.filter(
        entry => !entry.metadata.uploaded
      )
      const cachedAssigned = state.assigned.filter(
        entry => !entry.metadata.uploaded
      )

      await Promise.all(
        cachedRegistered.map((entry, index) =>
          db
            .collection('people')
            .doc(entry.data.id)
            .set(entry.data)
            .then(() => {
              commit('uploadedPerson', index)
            })
        )
      )
      await Promise.all(
        cachedAssigned.map((entry, index) =>
          db
            .collection('assignments')
            .doc(entry.data.id)
            .set(entry.data)
            .then(() => {
              commit('uploadedAssignment', index)
            })
        )
      )

      const registeredLocal = state.registered.map(entry => entry.data.id)
      const registeredSnapshot = await db.collection('people').get()
      registeredSnapshot.docs.forEach(doc => {
        if (!registeredLocal.includes(doc.id)) {
          const data = doc.data()
          commit('downloadedPerson', data)
        }
      })

      const assignedLocal = state.assigned.map(entry => entry.data.id)
      const assignedSnapshot = await db.collection('assignments').get()
      assignedSnapshot.docs.forEach(doc => {
        if (!assignedLocal.includes(doc.id)) {
          const data = doc.data()
          commit('downloadedAssignment', data)
        }
      })

      commit('sortAssignments')

      alert('Finished Syncing!')
    }
  }
})
