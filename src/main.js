import './registerServiceWorker'

import Vue from 'vue'
import VueFuse from 'vue-fuse'
import Multiselect from 'vue-multiselect'
import VueOnlinePlugin from 'vue-navigator-online'
import VueTheMask from 'vue-the-mask'
import VeeValidate from 'vee-validate'

import App from './App.vue'
import router from './router'
import store from './store'

// register globally

Vue.use(VeeValidate, {
  classes: true
})
Vue.use(VueTheMask)
Vue.use(VueFuse)
Vue.component('multiselect', Multiselect)
Vue.use(VueOnlinePlugin)

Vue.config.productionTip = false

new Vue({ router, store, render: h => h(App) }).$mount('#app')
